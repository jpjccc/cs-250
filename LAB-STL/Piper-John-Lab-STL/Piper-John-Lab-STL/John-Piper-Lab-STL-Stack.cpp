/*

// Lab - Standard Template Library - Part 4 - Stacks
// John, Piper

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> letters;
	bool user_done = false;
	cout << "Enter the next letter of the word, or UNDO to undo, or DONE to stop." << endl;
	while (user_done == false)
	{
		string input;
		cin >> input;

		if (input == "UNDO")
		{
			cout << "Removed: " << letters.top() << endl;
			letters.pop();
		}
		else if (input == "DONE")
		{
			user_done = true;
		}
		else
		{
			letters.push(input);
			//cout << "Letter: " << input << " added to the stack." << endl;

		}
	}
	cout << "Finished Word: ";
	while (!letters.empty())
	{
		cout << letters.top();
		letters.pop();
	}

	return 0;
}
*/
