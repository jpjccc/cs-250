// Lab - Standard Template Library - Part 5 - Maps
// John, Piper

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['y'] = "FF00FF";
	colors['m'] = "FFFF00";

	bool user_done = false;
	while (!user_done)
	{
		cout << endl << "Enter a color letter, or 'q' to stop: ";
		char input;
		cin >> input;
		if (input == 'q')
		{
			user_done = true;
		}
		else
		{
			cout << "Hex: " << colors[input] << endl;
		}
	}
	cout << "Goodbye!" << endl;
	return 0;
}