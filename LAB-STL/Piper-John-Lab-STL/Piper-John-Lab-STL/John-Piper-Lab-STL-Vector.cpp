/*
// Lab - Standard Template Library - Part 1 - Vectors
// John, Piper

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses; //string[] courses

	bool done = false;
	while (!done)
	{
		cout << "Course List Size: " << courses.size();
		cout << endl << "-------------------" << endl 
			<< "1. Add a new course" << '\t' 
			<< "2. Remove the last course" << '\t'
			<< "3. Display the course list" << '\t'
			<< "4. Quit" << endl;

		int choice;
		cin >> choice;

		if (choice == 1) //add course
		{
			string courseName; //temp variable
			cout << "NEW COURSE" << endl << "Enter new course name :";
			cin >> courseName;
			courses.push_back(courseName); //Add to list of courses

		}
		else if (choice == 2) //remove course
		{
			courses.pop_back(); //Remove last line item of courses
		}
		else if (choice == 3) //display list
		{
			cout << "VIEW COURSE LIST" << endl;
			for (unsigned int i = 0; i < courses.size(); i++)
			{
				cout << i << ". " << courses[i] << endl;
			}

		}
		else if (choice == 4) //quit
		{
			done = true;
		}
	}
	cout << "Goodbye!";
	return 0;
}
*/
