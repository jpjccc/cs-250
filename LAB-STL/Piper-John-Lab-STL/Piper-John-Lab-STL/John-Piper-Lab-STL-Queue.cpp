/*
// Lab - Standard Template Library - Part 3 - Queue
// John, Piper

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> tranaction_log;

	bool user_done = false;
	while (user_done == false)
	{
		cout << "----------------------------" << endl;
		cout << "Transactions queued: " << tranaction_log.size() << endl;
		cout << "1. Enqueue transaction 2. Continue" << endl;
		float input;
		cin >> input;
		if (input == 1)
		{
			cout << "Enter amount ( positive or negative ) for next transaction: " << endl;
			cin >> input;
			tranaction_log.push(input);
		}
		else if (input == 2)
		{
			user_done = true;
		}
		else
		{
			cout << "Invalid Input! Try again: " << endl;
		}
	}

	float balance = 0;
	cout << "----------------------------" << endl;
	while (tranaction_log.empty() == false)
	{
		cout << tranaction_log.front() << " pushed to account." << endl;
		balance += tranaction_log.front();
		tranaction_log.pop();
	}

	cout << endl << "Final Balance: $" << balance << endl;
	cout << "Goodbye" << endl;

	return 0;
}
*/