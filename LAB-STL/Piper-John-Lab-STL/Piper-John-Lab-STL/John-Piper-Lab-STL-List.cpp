/*
// Lab - Standard Template Library - Part 2 - Lists
// John, Piper

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states)
{
	for (
		//Initialization Code
		list<string>::iterator it = states.begin();
		//Test
		it != states.end();
		//Run after each interation
		it++
		)
	{
		//Display the element in the list
		cout << *it << " \t";
	}
}

int main()
{
	list<string> states;

	bool done = false;
	while (!done)
	{
		cout << '\n' << "--------------" << endl 
			<< "State list size: " << states.size() << endl;
		
		cout << "1. Add new state to front" << '\t' 
			<< "2. Add new state to back" << '\t'
			<< "3. Pop Front State" << '\t' 
			<< "4. Pop Back State" << '\t' 
			<< "5. Continue " << '\n'
			<< endl;

		int choice;
		cin >> choice;

		switch (choice)
		{
		case 1:			//push front
		{
			cout << "ADD STATE TO FRONT" << endl << "Enter a New State: ";
			string state;
			cin >> state;
			states.push_front(state);
			break;
		}
		case 2:			//push back
		{
			
			cout << "ADD STATE TO BACK" << endl << "Enter a New State: ";
			string state;
			cin >> state;
			states.push_back(state);
			break;
		}
		case 3:			//pop front
		{
			cout << "REMOVE STATE FROM FRONT" << endl << states.front() << " removed.";
			states.pop_front();
			break;
		}
		case 4:			//pop back
		{
			cout << "REMOVE STATE FROM Back" << endl << states.back() << " removed.";
			states.pop_back();
			break;
		}
		case 5:			//continue
		{
			done = true;
		}
		}
	}
	cout << "Original List: " << endl;
	DisplayList(states);
	cout << endl << "------------------------------" << endl;
	
	cout << "Reserved List: " << endl;
	states.reverse();
	DisplayList(states);
	cout << endl << "------------------------------" << endl;

	cout << "Sorted List: " << endl;
	states.sort();
	DisplayList(states);
	cout << endl << "------------------------------" << endl;

	cout << "Sorted-Reverse: " << endl;
	states.reverse();
	DisplayList(states);
	cout << endl << "Goodbye!" << endl;

	//cin.ignore();
	//cin.get();
	return 0;
}
*/
