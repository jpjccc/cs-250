// Lab - Standard Template Library - Part 2 - Lists
// FIRSTNAME, LASTNAME

//Austins Version
#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states)
{
	for (
		//Initialization Code
		list<string>::iterator it = states.begin();
		//Test
		it != states.end();
		//Run after each interation
		it++
		)
	{
		//Display the element in the list
		cout << *it << " \t";
	}
}

int main()
{
	list<string> states;
	
	bool done = false;
	while (!done)
	{
		cout << "1. Push Front " << "2. Push Back "
			<< "3. Pop Front " << "4. Pop Back " <<
			"5. Continue " << endl;
		int choice;
		cin >> choice;

		switch (choice)
		{
		case 1:			//push front
		{
			cout << "Enter a New State: ";
			string state;
			cin >> state;
			states.push_front(state);
			break;
		}
		case 2:			//push back
		{
			cout << "Enter a New State: ";
			string state;
			cin >> state;
			states.push_back(state);
			break;
		}
		case 3:			//pop front
		{
			cout << "Front Popped!";
			states.pop_front();
			break;
		}
		case 4:			//pop back
		{
			cout << "Back Popped!";
			states.pop_back();
			break;	
		}
		case 5:			//continue
		{
			done=true;
		}
		}
	}
	cout << "Original: " << endl;
	DisplayList(states);
	cout << endl << "------------------------------" << endl;

	cout << "Reserved: " << endl;
	states.reverse();
	DisplayList(states);
	cout << endl << "------------------------------" << endl;

	cout << "Sorted: " << endl;
	states.sort();
	DisplayList(states);
	cout << endl << "------------------------------" << endl;

	cout << "Sorted-Reverse: " << endl;
	states.reverse();
	DisplayList(states);
	cout << endl << "------------------------------" << endl;

    cin.ignore();
    cin.get();
    return 0;
}
