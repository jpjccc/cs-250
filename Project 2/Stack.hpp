#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"
#include "LinkedList.hpp"

template <typename T>
class LinkedStack// : public LinkedList<T>
{
    public:
	LinkedStack()// : LinkedList<T>()
    {
    }

    void Push( const T& newData )
    {
		// Allocating memory
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;

		// Place the node in the list:
		// List is empty
		if (m_ptrLast == nullptr)
		{
			// Point the first & last ptrs to the new node
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}

		// List has at least one item
		else
		{
			// Point the last node's ptrNext to the new node
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
		}

		// Increment item count:
		m_itemCount++;
	}

    T& Top()
    {
		if (m_ptrLast == nullptr)
		{
			throw out_of_range("Can't GetBack() for empty list!");
		}

		return m_ptrLast->data;
	}

    void Pop()
    {
		if (m_ptrFirst == nullptr)
		{
			// List is empty; ignore
			return;
		}

		// there is only one item in the list
		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}

		// there is more than one item in the list
		else
		{
			// Locate the second-to-last item
			Node<T>* ptrSecondToLast = m_ptrLast->ptrPrev;

			// Free the memory for the last item
			delete m_ptrLast;

			// Set the new last item
			m_ptrLast = ptrSecondToLast;

			// Set the last item's ptrNext to nullptr
			m_ptrLast->ptrNext = nullptr;
		}

		m_itemCount--;
    }

    int Size()
    {
		return m_itemCount;
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
