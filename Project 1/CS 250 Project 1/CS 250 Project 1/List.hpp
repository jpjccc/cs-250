#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;

template <typename T> //T is for Template
class List
{
private:
	void PassFailFunc(int a, int b)
	{
		if (a == b)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}


	// private member variables
	int m_itemCount;
	T m_arr[ARRAY_SIZE];

	// functions for interal-workings
	bool ShiftRight(int atIndex)
	{
		if (m_itemCount == 0)
		{
			return false;
		}
		else
		{
			for (int i = m_itemCount; i > atIndex; i--)
			{
				m_arr[i] = m_arr[i - 1];

			}
			return true;
		}
		
	}

	bool ShiftLeft(int atIndex)
	{
		if (m_itemCount == 0)
		{
			return false;
		}
		else
		{
			for (int i = 0; i < atIndex; i++)
			{
				m_arr[i] = m_arr[i + 1];
			}
			return true;
		}
	}

public:
	List()
	{
		m_itemCount = 0;
	}

	~List()
	{
	}

	// Core functionality
	int     Size() const
	{
		return this->m_itemCount;
	}

	bool    IsEmpty() const
	{
		return (m_itemCount == 0);
	}

	bool    IsFull() const
	{
		return (m_itemCount == ARRAY_SIZE);
	}

	bool    PushFront(T newItem)
	{
		if (IsFull())
		{
			return false;
		}

		ShiftRight(0);
		m_arr[0] = newItem;
		m_itemCount++;
	}

	bool	PushBack(T newItem)
	{
		if (IsFull())
		{
			return false;
		}
		m_arr[m_itemCount] = newItem;
		m_itemCount++;
	}

	bool	Insert(int atIndex, T item)
	{
		if (atIndex == 0)
		{
			return PushFront(item);
		}
		else if (atIndex == m_itemCount)
		{
			PushBack(item);
		}
		ShiftRight(atIndex);
		m_arr[atIndex] = item;
		m_itemCount++;
		return true;
	}

	bool    PopFront()
	{
		if (IsEmpty())
		{
			return false;
		}
		ShiftLeft(0);
		m_itemCount--;
		return true;
	}

	bool    PopBack()
	{
		if (IsEmpty())
		{
			return false;
		}
		m_itemCount--;
return true;
	}

	bool    RemoveItem(T item)
	{
		if (IsEmpty())
		{
			return false;
		}
		
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
			{
				m_arr[i] = 0;
			}
			return true;
		}
	}

	bool    RemoveIndex(int atIndex)
	{
		if (IsEmpty())
		{
			return false;
		}
		if (atIndex == 0)
		{
			ShiftLeft(0);
			m_itemCount--;
			return true;
		}
		else if (atIndex == m_itemCount - 1)
		{
			m_itemCount--;
			return true;
		}
		ShiftLeft(atIndex);
		m_itemCount--;
		return true;

	}

	void    Clear()
	{
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (IsEmpty() && atIndex >= m_itemCount)
		{
			return nullptr;
		}
		return &m_arr[atIndex];
	}

	T*      GetFront()
	{
		if (IsEmpty())
		{
			return nullptr; // placeholder
		}
		return &m_arr[0];
	}

	T*      GetBack()
	{
		if (IsEmpty())
		{
			return nullptr; // placeholder
		}
		return &m_arr[m_itemCount];
	}

	// Additional functionality
	int     GetCountOf(T item) const
	{
		int counter = 0;
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
			{
				counter++;
			}
		}
		return counter;
	}

	bool    Contains(T item) const
	{
		if (GetCountOf(item) > 0)
		{
		return true;
		}
		else
		{
			return false;
		}
	}

	void Display()
	{
		cout << "\t List Size: " << Size() << endl;
		for (int i = 0; i < Size(); i++)
		{
			T* item = Get(i);
			cout << "\t " << i << "=";

			if (item == nullptr)
			{
				cout << "nullptr" << endl;
			}
			else
			{
				cout << *item << endl;
			}
		}
	}

    friend class Tester;
};


#endif
