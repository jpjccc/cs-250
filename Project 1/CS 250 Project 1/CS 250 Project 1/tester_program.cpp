#include <iostream>
#include <string>
#include "Tester.hpp"

using namespace std;

int main()
{
    Tester tester;
    tester.RunTests();

	cin.get();
	cin.ignore();

    return 0;
}


