#include "Tester.hpp"

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;
	//Need to insert here
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

	{
		//Test 1
		List<int> testList;
		for (int i = 1; i <= 5; i++)
		{
			testList.PushBack(i);
		}
		bool expected = true;
		cout << "Test 1" << '\n' << "1. Create a list with 5 items. ShiftRight 2 places." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.ShiftRight(2) << endl;
		testList.PassFailFunc(expected, testList.ShiftRight(2));
	}
	{
		//Test 2
		List<int> testList;
		bool expected = false;
		cout << "Test 2" << '\n' << "2. Create a list with no items. ShiftRight 5 places." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.ShiftRight(5) << endl;
		testList.PassFailFunc(expected, testList.ShiftRight(5));
	}
	{
		//Test 3
		List<int> testList;
		testList.PushBack(0);
		bool expected = true;
		cout << "Test 3" << '\n' << "3. Create a list with 1 items. ShiftRight 0 places." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.ShiftRight(0) << endl;
		testList.PassFailFunc(expected, testList.ShiftRight(0));
	}

}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;
	{
		//Test 1
		List<int> testList;
		for (int i = 1; i <= 5; i++)
		{
			testList.PushBack(i);
		}
		bool expected = true;
		cout << "Test 1" << '\n' << "1. Create a list with 5 items. ShiftLeft 2 places." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.ShiftLeft (2) << endl;
		testList.PassFailFunc(expected, testList.ShiftLeft(2));
	}
	{
		//Test 2
		List<int> testList;
		bool expected = false;
		cout << "Test 2" << '\n' << "2. Create a list with no items. ShiftLeft 5 places." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.ShiftLeft(2) << endl;
		testList.PassFailFunc(expected, testList.ShiftLeft(5));
	}
	{
		//Test 3
		List<int> testList;
		testList.PushBack(0);
		bool expected = true;
		cout << "Test 3" << '\n' << "3. Create a list with 1 items. ShiftLeft 0 places." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.ShiftLeft(2) << endl;
		testList.PassFailFunc(expected, testList.ShiftLeft(0));
	}
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

		testList.PassFailFunc(expectedSize, actualSize);
    }   // Test end

    {   // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;
		testList.PassFailFunc(actualSize, expectedSize);

    }   // Test end

	{   // Test begin
		cout << endl << "Test 3" << endl;
		List<int> testList;
		
		for (int i = 1; i <= 10; i++) // Populate the list item
		{
			testList.PushBack(i);
		}

		int expectedSize = 10;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		testList.PassFailFunc(actualSize, expectedSize);

	}   // Test end
	{   // Test begin
		cout << endl << "Test 4" << endl;
		List<int> testList;

		for (int i = 1; i <= 100; i++) // Populate the list item
		{
			testList.PushBack(i);
		}

		int expectedSize = 100;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;
		
		testList.PassFailFunc(actualSize, expectedSize);
	}   // Test end
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;
	{
		//Test 1
		List<int> testList;
		int expected = 1;
		cout << "Test 1" << '\n' << "1. Create a List, with no items." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.IsEmpty();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 2
		List<int> testList;
		testList.PushFront(1);
		// 0  is false
		int expected = 0;
		cout << "Test 2" << '\n' << "2. Create a List, with 1 item." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.IsEmpty();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 3
		List<int> testList;
		for (int i = 0; i < 5; i++)
		{
			testList.PushFront(i);
		}
		int expected = 0;
		cout << "Test 3" << '\n' << "3. Create a List, with 5 items." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.IsEmpty();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}

}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;
	{
		//Test 2
		List<int> testList;
		int expected = 0;
		cout << "Test 1" << '\n' << "1. Create a List, with no items." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.IsFull();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 2
		List<int> testList;
		testList.PushFront(5);
		testList.PushFront(7);
		int expected = 0;
		cout << "Test 2" << '\n' << "2. Create a List, with 2 items." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.IsFull();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 3
		List<int> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushFront(i);
		}
		int expected = 1;
		cout << "Test 3" << '\n' << "3. Create a List, with 100 items." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.IsFull();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 4
		List<int> testList;
		for (int i = 0; i < 105; i++)
		{
			testList.PushFront(i);
		}
		int expected = 1;
		cout << "Test 3" << '\n' << "3. Create a List, with 105 items." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.IsFull();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;
	{
		//Test 1
		List<int> testList;
		testList.PushFront(1);
		int expected = 1;
		cout << "Test 1" << '\n' << "1. Create a List, insert 1 item." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 2
		List<int> testList;
		testList.PushFront(1);
		testList.PushBack(2);
		int expected = 2;
		cout << "Test 2" << '\n' << "2. Create a List, insert 2 items." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;
	{
		//Test 1
		List<int> testList;
		testList.PushBack(1);
		int expected = 1;
		cout << "Test 1" << '\n' << "1. Create a List, pushBack 1 item." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 2
		List<int> testList;
		testList.PushBack(1);
		testList.PushBack(2);
		int expected = 2;
		cout << "Test 2" << '\n' << "2. Create a List, pushBack 2 items." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 3
		List<int> testList;
		for (int i = 0; i < 100; i++)
		{
			testList.PushBack(i);
		}
		int expected = 100;
		cout << "Test 3" << '\n' << "3. Create a List, pushBack 100 items." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;
	{
		//Test 1
		List<int> testList;
		for (int i = 0; i < 20; i++)
		{
			testList.PushBack(i);
		}
		testList.PopFront();
		int expected = 19;
		cout << "Test 1" << '\n' << "1. Create a List, PopFront 1 item." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 2
		List<int> testList;
		for (int i = 0; i < 20; i++)
		{
			testList.PushBack(i);
		}
		testList.PopFront();
		testList.PopFront();
		int expected = 18;
		cout << "Test 2" << '\n' << "2. Create a List, PopFront 2 items." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 3
		List<int> testList;
		for (int i = 0; i < 20; i++)
		{
			testList.PushFront(i);
		}
		for (int i = 0; i < 21; i++)
		{
			testList.PopFront();
		}		
		int expected = 0;
		cout << "Test 3" << '\n' << "3. Create a List, PopFront 21 items." << "\n" << "Expected Result: " << expected << endl;
		bool result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;
	{
		//Test 1
		List<int> testList;
		for (int i = 0; i < 20; i++)
		{
			testList.PushBack(i);
		}
		testList.PopBack();
		int expected = 19;
		cout << "Test 1" << '\n' << "1. Create a List, PopBack 1 item." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 2
		List<int> testList;
		for (int i = 0; i < 20; i++)
		{
			testList.PushBack(i);
		}
		testList.PopBack();
		testList.PopBack();
		int expected = 18;
		cout << "Test 2" << '\n' << "2. Create a List, PopBack 2 items." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 3
		List<int> testList;
		for (int i = 0; i < 20; i++)
		{
			testList.PushBack(i);
		}
		for (int i = 0; i < 21; i++)
		{
			testList.PopBack();
		}
		int expected = 0;
		cout << "Test 3" << '\n' << "3. Create a List, PopBack 21 items." << "\n" << "Expected Result: " << expected << endl;
		int result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;
	{
		//Test 1
		List<int> testList;
		testList.PushBack(7);
		int expected = 0;
		cout << "Test 1" << '\n' << "1. Create a List, Clear 1 item." << "\n" << "Expected Result: " << expected << endl;
		testList.Clear();
		bool result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
	{
		//Test 2
		List<int> testList;
		int expected = 0;
		cout << "Test 2" << '\n' << "2. Create a List, Clear with 0 items." << "\n" << "Expected Result: " << expected << endl;
		testList.Clear();
		bool result = testList.Size();
		cout << "Actual Result: " << result << endl;
		testList.PassFailFunc(expected, result);
	}
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;
	{
		//Test 1
		List<int> testList;
		for (int i = 0; i < 5; i++)
		{
			testList.PushBack(i);
		}
		cout << "Test 1" << '\n' << "Create a list with 5 items" << "\n" << "Expected Result: (memory address)" << endl;
		cout << "Actual Result: " << testList.Get(5) << endl << "Pass" << endl;
	}
	{
		//Test 2
		List<int> testList;
		cout << endl << "Test 2" << '\n' << "Create a list with no items" << "\n" << "Expected Result: (memory address)" << endl;
		cout << "Actual Result: " << testList.Get(0) << endl << "Pass" << endl;
	}
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;
	{
		//Test 1
		List<int> testList;
		testList.PushBack(5);
		cout << "Test 1" << '\n' << "1. Create a list with 1 item" << "\n" << "Expected Result: " << testList.Get(0) << endl << endl;
		cout << "Actual Result: " << testList.GetFront() << endl;
		if (testList.GetFront() == testList.Get(0))
		{
			cout << "Pass";
		}
		else
		{
			cout << "Fail";
		}
		{
			//Test 2
			List<int> testList;
			cout << endl << "Test 2" << '\n' << "2. Create a list with no items" << "\n" << "Expected Result: " << testList.Get(0) << endl << endl;
			cout << "Actual Result: " << testList.GetFront() << endl;
			if (testList.GetFront() == testList.Get(0))
			{
				cout << "Pass";
			}
			else
			{
				cout << "Fail";
			}
		}
	}
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

	{
		//Test 1
		List<int> testList;
		testList.PushFront(5);
		cout << "Test 1" << '\n' << "1. Create a list with 1 item" << "\n" << "Expected Result: " << testList.Get(1) << endl << endl;
		cout << "Actual Result: " << testList.GetBack() << endl;
		if (testList.GetBack() == testList.Get(1))
		{
			cout << "Pass";
		}
		else
		{
			cout << "Fail";
		}
	}
	{
		//Test 2
		List<int> testList;
		cout << endl << "Test 2" << '\n' << "2. Create a list with no item" << "\n" << "Expected Result: " << testList.Get(0) << endl << endl;
		cout << "Actual Result: " << testList.GetBack() << endl;
		if (testList.GetBack() == testList.Get(0))
		{
			cout << "Pass";
		}
		else
		{
			cout << "Fail";
		}
	}
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;
	
	{
		//Test 1
		List<int> testList;
		int expected = 0;
		cout << "Test 1" << '\n' << "1. Create a list with no items." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.GetCountOf(5) << endl;
		testList.PassFailFunc(expected, testList.GetCountOf(5));
	}
	{
		//Test 2
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(3);
		testList.PushFront(5);
		int expected = 1;
		cout << "Test 2" << '\n' << "2. Create a list with 1,3 and 5, search for 5." << "\n" << "Expected Result: " << expected  << endl;
		cout << "Actual Result: " << testList.GetCountOf(5) << endl;
		testList.PassFailFunc(expected, testList.GetCountOf(5));
	}
	{
		//Test 3
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(5);
		testList.PushFront(5);
		int expected = 2;
		cout << "Test 3" << '\n' << "3. Create a list with 1,5 and 5, search for 5." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.GetCountOf(5) << endl;
		testList.PassFailFunc(expected, testList.GetCountOf(5));
	}
	{
		//Test 4
		List<int> testList;
		testList.PushFront(5);
		testList.PushFront(5);
		testList.PushFront(5);
		testList.PushFront(5);
		int expected = 4;
		cout << "Test 4" << '\n' << "4. Create a list with 5,5,5 and 5, search for 5." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.GetCountOf(5) << endl;
		testList.PassFailFunc(expected, testList.GetCountOf(5));
	}
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

	{
		//Test 1
		List<int> testList;
		bool expected = false;
		cout << "Test 1" << '\n' << "1. Create a list with no items." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.Contains(1) << endl;
		testList.PassFailFunc(expected, testList.Contains(1));
	}
	{
		//Test 2
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		bool expected = false;
		cout << "Test 2" << '\n' << "2. Create a list with 1,2 and 3 items. Search for 4." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.Contains(4) << endl;
		testList.PassFailFunc(expected, testList.Contains(4));
	}
	{
		//Test 3
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		bool expected = true;
		cout << "Test 3" << '\n' << "3. Create a list with 1,2 and 3 items. Search for 2." << "\n" << "Expected Result: " << expected << endl;
		cout << "Actual Result: " << testList.Contains(2) << endl;
		testList.PassFailFunc(expected, testList.Contains(2));
	}
}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

	List<int> testList; // Create a List Item
	for (int i = 1; i <= 3; i++) // Populate the list item
	{
		testList.PushBack(i);
	}
	cout << "Array before remove function: " << endl;
	testList.Display();
	cout << "Removing the 3rd item..." << endl;
	testList.RemoveIndex(2);
	cout << "Array after remove function: " << endl;
	testList.Display();
}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;
	
	List<int> testList;
	for (int i = 1; i <= 2; i++)
	{
		testList.PushBack(i);
	}
	cout << "Array before Insert function: " << endl;
	testList.Display();
	cout << "Inserting a 2nd item..." << endl;
	testList.Insert(1, 7);
	cout << "Array after insert function: " << endl;
	testList.Display();
}
