#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
public:
	void FirstComeFirstServe(vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile);
	void RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile);
};

void Processor::FirstComeFirstServe(vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile)
{	
	int counter = 0;
	ofstream output;
	output.open(logFile);
	output << "First Come First Served ( FCFS )" << endl;
	output << "Processing job #" << counter << "..." << endl;

	while (jobQueue.Size() != 0)
	{
		jobQueue.Front()->Work(FCFS);

		counter++;
		output << "CYCLE \t" << counter << "\t REMAINING : \t" << jobQueue.Front()->fcfs_timeRemaining << endl;
		
		if (jobQueue.Front()->fcfs_done == true)
		{
			jobQueue.Front()->SetFinishTime(counter, FCFS);
			jobQueue.Pop();
		}
	}

	output << "Job ID \t" << "TIME TO COMPLETE" << endl;
	int sum = 0;
	for (int i = 0; i < allJobs.size(); i++)
	{
		output << allJobs[i].id << "\t" << allJobs[i].fcfs_finishTime << endl;
		sum += allJobs[i].fcfs_finishTime;
	}
	output << "Total time : \t" << counter + 1 << endl << "( Time for all jobs to complete processing )" << endl;
	output << "Average time: \t" << (sum / allJobs.size()) << endl << 
		"( The average time to complete , including the wait time while items are ahead of it in the queue.)";
	output.close();	

}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	int cycles = 0;
	int timer = 0;
	ofstream output;
	output.open(logFile);
	output << "First Come First Served ( RR )" << endl;
	output << "Processing job #" << timer << "..." << endl;

	while (jobQueue.Size() != 0)
	{

		output << "CYCLE \t" << timer << "\t REMAINING : \t" << jobQueue.Front()->rr_timeRemaining << endl;

		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
		}
	
			jobQueue.Front()->Work(RR);
			timer++;
			if (jobQueue.Front()->rr_done == true)
				{
					jobQueue.Front()->SetFinishTime(cycles, RR);
					jobQueue.Pop();
				}
			cycles++;
	}

	output << "Job ID \t" << "TIME TO COMPLETE" << endl;
	int sum = 0;
	for (int i = 0; i < allJobs.size(); i++)
	{
		output << allJobs[i].id << "\t" << allJobs[i].rr_finishTime << endl;
		sum += allJobs[i].rr_finishTime;
	}
	output << "Total time : \t" << cycles + 1 << endl << "( Time for all jobs to complete processing )" << endl;
	output << "Average time: \t" << (sum / allJobs.size()) << endl <<
		"( The average time to complete , including the wait time while items are ahead of it in the queue.)";
	output.close();

}

#endif
